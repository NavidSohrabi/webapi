﻿using Newtonsoft.Json.Linq;
using Store.Model;
using Store.Services.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;

namespace Store.Services.Controllers
{
    public class ProductController : ApiController
    {
        private readonly Storedb db;
        public ProductController()
        {
            db = new Storedb();
        }

        //دریافت لیست تمامی کالاها

        [HttpGet]
        public IHttpActionResult Products()
        {
            var list = new List<Product>();
            list = db.Products.ToList();
            return Ok(list);
        }

        //دریافت کالایی با آی دی مشخص

        [HttpGet]
        public IHttpActionResult GetIdProduct(int Id)
        {
            var Entity = db.Products.Find(Id);
            return Ok(Entity);
        }

        //دریافت لیست تمامی کشورها

        [HttpGet]
        public IHttpActionResult GetCountries()
        {
            var list = new List<Country>();
            list = db.Countries.ToList();
            return Ok(list);
        }

        //دریافت لیست دسته بندی ها

        [HttpGet]
        public IHttpActionResult GetCategory()
        {
            var list = new List<Category>();
            list = db.Categories.ToList();
            return Ok(list);
        }
        
        //دریافت لیست کمپانی هایی که حداقل یک کالا تولید کرده باشند، با استفاده از استورد پراسیجر

        public class SP1
        {
            public string PersianName { get; set; }
        }
        [HttpGet]
        public IHttpActionResult GetProducers()
        {
            {
                var result = db.Database.SqlQuery<SP1>("SP1").ToList();
                return Ok(result);
            }
        }
        
        //پست کردن دیتا در کالاها

        [HttpPost]
        public IHttpActionResult PostProducts(int Catid, int Comid, string Modelna, string Desc)
        {
            var entity = new Product()
            {
                CategoryId = Catid,
                CompanyId = Comid,
                ModelName = Modelna,
                Description = Desc
            };

            db.Products.Add(entity);
            db.SaveChanges();
            return Ok(entity);
        }

    }

}